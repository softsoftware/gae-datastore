package net.ass;

import net.ass.entity.EntityFactory;

/**
 * Created by erickmaldonado on 11/12/16.
 */
public class DatastoreServiceDaoFactory {

    private static EntityFactory entityFactory;
    private static DatastoreServiceDao datastoreServiceDao;

    public static EntityFactory getEntityFactory() {
        return entityFactory;
    }

    public static void setEntityFactory(EntityFactory entityFactory) {
        DatastoreServiceDaoFactory.entityFactory = entityFactory;
    }

    public static DatastoreServiceDao getDatastoreServiceDao(String namespace) {
        DatastoreServiceDao datastoreServiceDao = new DatastoreServiceDao(namespace, entityFactory);
        return datastoreServiceDao;
    }

    public static DatastoreEntityServiceDao getDatastoreServiceDao(String namespace, String entityName ) {
        DatastoreEntityServiceDao datastoreServiceDao = new DatastoreEntityServiceDao(namespace, entityFactory, entityName);
        return datastoreServiceDao;
    }
}
