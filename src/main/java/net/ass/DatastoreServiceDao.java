package net.ass;

import java.util.*;
import java.util.logging.Logger;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.datastore.*;
import net.ass.entity.*;
import net.ass.exception.EntityExistException;
import net.ass.exception.InvalidPropertyException;
import net.ass.exception.MissingKeyException;
import net.ass.exception.NotIndexedException;


public class DatastoreServiceDao {

    private static final Logger log = Logger.getLogger(DatastoreServiceDao.class.getName());
    private DatastoreService datastore;
    private FetchOptions fetchOptions;
    private EntityFactory entityFactory;

    public EntityFactory getEntityFactory() {
        return entityFactory;
    }

    public void setEntityFactory(EntityFactory entityValidator) {
        this.entityFactory = entityValidator;
    }

    public DatastoreServiceDao (String namespace, EntityFactory entityFactory) {
        String currentNamespace = NamespaceManager.get();
        log.info("Current Namespace: [" +  currentNamespace + "]");
        if ((namespace != null) && (namespace.length() > 0)) {
            log.info("Changing to Namespace: [" +  namespace + "]");
            NamespaceManager.set(namespace);
        }
        else {
            NamespaceManager.set("");
        }
        this.datastore = DatastoreServiceFactory.getDatastoreService();
        fetchOptions = FetchOptions.Builder.withDefaults();
        this.entityFactory = entityFactory;
    }

    public Map<String, Object> createEntity(EntityDefinition entityDefinition, Map<String, Object> propertiesMap)
            throws InvalidPropertyException, EntityExistException {
        entityDefinition.getValidators().beforePost(propertiesMap);
        Object keyValue = propertiesMap.get(entityDefinition.getKeyName());
        propertiesMap.remove(entityDefinition.getKeyName());
        propertiesMap = validatePropertiesMap(entityDefinition, propertiesMap);

        Key key = null;
        if (keyValue != null) {
            if (entityDefinition.getKeyType().equals(KeyType.String)) {
                key = KeyFactory.createKey(entityDefinition.getKind(), (String) keyValue);
            } else if (entityDefinition.getKeyType().equals(KeyType.Long)) {
                Long longKey = Long.valueOf(String.valueOf(keyValue));
                if (longKey != null) {
                    key = KeyFactory.createKey(entityDefinition.getKind(), longKey);
                }
            }
        }

        if (key == null) {
            Entity entity = new Entity(entityDefinition.getKind());
            entity = populateEntity(entityDefinition, entity, propertiesMap, true);
            datastore.put(entity);
            Map<String, Object> result = entityDefinition.getValidators().afterPost(getPropertyMap(entityDefinition, entity));
            return result;
        }

        try {
            datastore.get(key);
            throw new EntityExistException("Entity [" + entityDefinition.getKind() + "] with key [" + key.getName() + "/" + key.getId() + "] already exist");
        } catch (EntityNotFoundException e) {
            Entity entity = new Entity(key);
            entity = populateEntity(entityDefinition, entity, propertiesMap, true);
            datastore.put(entity);
            Map<String, Object> result = entityDefinition.getValidators().afterPost(getPropertyMap(entityDefinition, entity));
            return result;
        }
    }

    public Map<String, Object> saveEntity(Object keyValue, EntityDefinition entityDefinition, Map<String, Object> propertiesMap, boolean strict)
            throws InvalidPropertyException, EntityNotFoundException, MissingKeyException {
        entityDefinition.getValidators().beforePut(propertiesMap);
        propertiesMap = validatePropertiesMap(entityDefinition, propertiesMap);
        log.info(keyValue.toString());
        if (keyValue == null) {
            throw new MissingKeyException("Missing key [" + entityDefinition.getKeyName() + "] to retrieve this entity");
        }

        Key key = null;
        Entity entity;
        if (entityDefinition.getKeyType().equals(KeyType.String)) {
            key = KeyFactory.createKey(entityDefinition.getKind(), (String) keyValue);
        } else if (entityDefinition.getKeyType().equals(KeyType.Long)) {
            key = KeyFactory.createKey(entityDefinition.getKind(), Long.valueOf(String.valueOf(keyValue)));
        }
        entity = datastore.get(key);

        entity = populateEntity(entityDefinition, entity, propertiesMap, strict);
        datastore.put(entity);
        Map<String, Object> result = entityDefinition.getValidators().afterPut(getPropertyMap(entityDefinition, entity));
        return result;
    }

    public Map<String, Object> getEntity(EntityDefinition entityDefinition, Object id) throws EntityNotFoundException {
        Key key = null;
        if (entityDefinition.getKeyType().equals(KeyType.String)) {
            key = KeyFactory.createKey(entityDefinition.getKind(), (String) id);
        } else if (entityDefinition.getKeyType().equals(KeyType.Long)) {
            key = KeyFactory.createKey(entityDefinition.getKind(), Long.valueOf(String.valueOf(id)));
        }
        Entity entity = datastore.get(key);

        return entityDefinition.getValidators().afterGet(getPropertyMap(entityDefinition, entity));
    }

    public void deleteEntity(EntityDefinition entityDefinition, Object id) {
        Key key = null;
        if (entityDefinition.getKeyType().equals(KeyType.String)) {
            key = KeyFactory.createKey(entityDefinition.getKind(), (String) id);
        } else if (entityDefinition.getKeyType().equals(KeyType.Long)) {
            key = KeyFactory.createKey(entityDefinition.getKind(), Long.valueOf(String.valueOf(id)));
        }
        datastore.delete(key);
    }

    public CursorList<Map<String, Object>> getEntities(EntityDefinition entityDefinition, Query query) {
        QueryResultList<Entity> entities = datastore.prepare(query).asQueryResultList(fetchOptions);
        CursorList<Map<String, Object>> cursorList = new CursorList<>(getPropertiesMap(entityDefinition, entities), entities.getCursor().toWebSafeString());
        return cursorList;
    }

    public CursorList<Map<String, Object>> getEntities(EntityDefinition entityDefinition, List<String> stringKeys) {
        List<Key> keys = new ArrayList<>();
        for (String key : stringKeys) {
            if (entityDefinition.getKeyType().equals(KeyType.Long)) {
                keys.add(KeyFactory.createKey(entityDefinition.getKind(), Long.valueOf(key)));
            } else {
                keys.add(KeyFactory.createKey(entityDefinition.getKind(), key));
            }
        }
        Map<Key, Entity> entities = datastore.get(keys);
        List<Map<String, Object>> mapEntities = new ArrayList<>();
        for (Map.Entry<Key, Entity> entry : entities.entrySet()) {
            mapEntities.add(getPropertyMap(entityDefinition, entry.getValue()));
        }
        CursorList<Map<String, Object>> cursorList = new CursorList<>(mapEntities);
        return cursorList;
    }

    public CursorList<Map<String, Object>> getProjection(EntityDefinition entityDefinition, List<String> fields) throws NotIndexedException {
        QueryResultList<Entity> entities = datastore.prepare(getProjectionQuery(entityDefinition, fields)).asQueryResultList(fetchOptions);
        CursorList<Map<String, Object>> cursorList = new CursorList<>(getPropertiesMap(entityDefinition, entities), entities.getCursor().toWebSafeString());
        return cursorList;
    }

    private Query getQuery(EntityDefinition entityDefinition, Map<String, String> query) {
        return null;
    }

    private Entity populateEntity(EntityDefinition entityDefinition, Entity entity, Map<String, Object> propertiesMap, boolean strict) {
        List<String> propertyList = entityDefinition.getIndexedFields();

        for (String propertyName : propertyList) {
            Object propertyValue = propertiesMap.get(propertyName);
            if (propertyValue != null || strict) { //null and not strict will not add the property
                if (!propertyName.equals(entityDefinition.getKeyName())) {
                    boolean isPropertyIndexed = entityDefinition.getIndexedFields().contains(propertyName);
                    if (isPropertyIndexed) {
                        entity.setProperty(propertyName, propertyValue);
                    } else {
                        entity.setUnindexedProperty(propertyName, propertyValue);
                    }
                }
            }
        }
        return entity;
    }

    private Query getProjectionQuery(EntityDefinition entityDefinition, List<String> fields) throws NotIndexedException {
        Query query = new Query();
        List<String> notIndexedFields = new ArrayList<>();
        for (String field : fields) {
            if (!entityDefinition.getIndexedFields().contains(field)) {
                notIndexedFields.add(field);
            }
            query.addProjection(new PropertyProjection(field, entityDefinition.getFieldsMap().get(field)));
        }
        if (!notIndexedFields.isEmpty()) {
            throw new NotIndexedException(notIndexedFields);
        }
        return query;
    }


    private List<Map<String, Object>> getPropertiesMap(EntityDefinition entityDefinition, List<Entity> entities) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (Entity entity : entities) {
            mapList.add(getPropertyMap(entityDefinition, entity));
        }
        return mapList;
    }

    private Map<String, Object> getPropertyMap(EntityDefinition entityDefinition, Entity entity) {
        Map<String, Object> properties = new HashMap<>(entity.getProperties());
        if (entity.getKey().getName() == null) {
            properties.put(entityDefinition.getKeyName(), entity.getKey().getId());
        }
        else {
            properties.put(entityDefinition.getKeyName(), entity.getKey().getName());
        }
        return properties;
    }

    public Map<String, Object> validatePropertiesMap(EntityDefinition entityDefinition, Map<String, Object> propertiesMap) throws InvalidPropertyException {

        Set<Map.Entry<String, Object>> entries = propertiesMap.entrySet();
        List<String> invalidProperties = new ArrayList<>();

        for (Map.Entry<String, Object> entry : entries) {
            if (entityDefinition.getFieldsMap().get(entry.getKey()) == Date.class) {
                if (propertiesMap.get(entry.getKey()) instanceof Long) {
                    propertiesMap.put(entry.getKey(), new Date((Long)propertiesMap.get(entry.getKey())));
                }
            }
            if (!entityDefinition.getFields().contains(entry.getKey())) {
                invalidProperties.add("Invalid property [" + entry.getKey() + "]");
            }
        }

        if (!invalidProperties.isEmpty()) {
            throw new InvalidPropertyException(invalidProperties);
        }

        for (Map.Entry<String, Object> entry : entries) {
            Class propertyClass = entityDefinition.getFieldsMap().get(entry.getKey());
            if (propertyClass.equals(Long.class)){
            	propertiesMap.put(entry.getKey(), Long.valueOf(String.valueOf(entry.getValue())));
            }
        }

        return propertiesMap;
    }
}