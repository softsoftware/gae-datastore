package net.ass.exception;

/**
 * Created by erickmaldonado on 10/17/16.
 */
public class EntityExistException extends Exception {

    public EntityExistException (String message) {
        super(message);
    }

}
