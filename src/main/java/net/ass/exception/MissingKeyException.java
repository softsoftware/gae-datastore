package net.ass.exception;

/**
 * Created by erickmaldonado on 10/17/16.
 */
public class MissingKeyException extends Exception{

    public MissingKeyException(String message) {
        super(message);
    }
}
