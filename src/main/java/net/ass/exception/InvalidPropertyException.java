package net.ass.exception;

import java.util.List;

/**
 * Created by erickmaldonado on 10/16/16.
 */
public class InvalidPropertyException extends Exception {

    private List<String> fields;

    public InvalidPropertyException (List<String> fields) {
        this.fields = fields;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }
}
