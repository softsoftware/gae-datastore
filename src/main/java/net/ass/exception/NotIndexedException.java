package net.ass.exception;

import java.util.List;

/**
 * Created by erickmaldonado on 10/15/16.
 */
public class NotIndexedException extends Exception {

    private List<String> fields;

    public NotIndexedException (List<String> fields) {
        this.fields = fields;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }
}
