package net.ass;

import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Query;
import net.ass.entity.CursorList;
import net.ass.entity.EntityDefinition;
import net.ass.entity.EntityFactory;
import net.ass.exception.EntityExistException;
import net.ass.exception.InvalidPropertyException;
import net.ass.exception.MissingKeyException;
import net.ass.exception.NotIndexedException;

import java.util.List;
import java.util.Map;


/**
 * Created by erickmaldonado on 6/19/17.
 */
public class DatastoreEntityServiceDao extends DatastoreServiceDao {

    private EntityDefinition entityDefinition;

    public DatastoreEntityServiceDao (String namespace, EntityFactory entityFactory, String entityName) {
        super(namespace, entityFactory);
        entityDefinition = getEntityFactory().getEntityDefinition(entityName);
    }

    public Map<String, Object> createEntity(Map<String, Object> propertiesMap) throws InvalidPropertyException, EntityExistException {
        return this.createEntity(entityDefinition, propertiesMap);
    }

    public Map<String, Object> saveEntity(Object keyValue, Map<String, Object> propertiesMap, boolean strict)
            throws InvalidPropertyException, EntityNotFoundException, MissingKeyException {
        return this.saveEntity(keyValue, entityDefinition, propertiesMap, strict);
    }

    public Map<String, Object> getEntity(Object id) throws EntityNotFoundException {
        return this.getEntity(entityDefinition, id);
    }

    public void deleteEntity(Object id) {
        this.deleteEntity(entityDefinition, id);
    }

    public CursorList<Map<String, Object>> getEntities(Query query) {
        return this.getEntities(entityDefinition, query);
    }

    public CursorList<Map<String, Object>> getEntities(List<String> stringKeys) {
        return this.getEntities(entityDefinition, stringKeys);
    }

    public CursorList<Map<String, Object>> getProjection(List<String> fields) throws NotIndexedException {
        return this.getProjection(entityDefinition, fields);
    }

    public Map<String, Object> validatePropertiesMap(Map<String, Object> propertiesMap) throws InvalidPropertyException {
        return this.validatePropertiesMap(entityDefinition, propertiesMap);
    }

}
