package net.ass.entity;

import java.util.List;
import java.util.Map;

/**
 * Created by erickmaldonado on 10/14/16.
 */
public interface EntityDefinition {

    public String getKind();
    public Map<String, Class> getFieldsMap();
    public List<String> getFields();
    public List<String> getIndexedFields();
    public String getKeyName();
    public KeyType getKeyType();
    public EntityValidator getValidators();

}
