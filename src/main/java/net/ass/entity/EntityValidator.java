package net.ass.entity;

import java.util.List;
import java.util.Map;

/**
 * Created by erickmaldonado on 10/16/16.
 */
public class EntityValidator {

    public Map<String, Object> beforePost(Map<String, Object> properties) {
        return properties;
    }

    public Map<String, Object> afterPost(Map<String, Object> properties) {
        return properties;
    }

    public Map<String, Object> beforePut(Map<String, Object> properties) {
        return properties;
    }

    public Map<String, Object> afterPut(Map<String, Object> properties) {
        return properties;
    }

    public Map<String, Object> afterGet(Map<String,Object> got) {
        return got;
    }

    public List<Map<String, Object>> afterGet(List<Map<String,Object>> got, Map<String,Object> query){
        return got;
    }

}
