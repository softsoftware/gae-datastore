package net.ass.entity;

import java.util.List;

/**
 * Created by erickmaldonado on 10/15/16.
 */
public class CursorList<T> {

    private List<T> items;
    private String cursor;

    public CursorList(List<T> items, String cursor) {
        this.items = items;
        this.cursor = cursor;
    }

    public CursorList(List<T> items) {
        this.items = items;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public String getCursor() {
        return cursor;
    }
}
